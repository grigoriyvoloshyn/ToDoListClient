import React from "react";
import { Button, Modal, Form, Input, Checkbox } from "antd";
import { ITask, IUpdateTaskProps } from "./Types";
import { useTasks } from "../Store/Store";
import Api from "./Api";

const TaskEditForm = ({ visible, onCreate, onCancel, task }: any) => {
  const [form] = Form.useForm();
  return (
    <Modal
      visible={visible}
      title="Edit your task"
      okText="Edit"
      cancelText="Cancel"
      onCancel={() => {
        form.resetFields();
        onCancel();
      }}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            onCreate(values);
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: "public",
        }}
      >
        <Form.Item name="id" label="Id" initialValue={task.id} hidden={true}>
          <Input />
        </Form.Item>

        <Form.Item
          name="name"
          label="Name"
          initialValue={task.name}
          rules={[
            {
              required: true,
              message: "Please input name of the task!",
            },
            {
              max: 50,
              message: "Name of the task should be less than 50 characters",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="description"
          label="Description"
          initialValue={task.description}
          rules={[
            {
              max: 200,
              message: "Task description should be less than 200 characters",
            },
          ]}
        >
          <Input type="textarea" />
        </Form.Item>

        <Form.Item
          name="isCompleted"
          valuePropName="checked"
          initialValue={task.isCompleted}
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Checkbox>Task completed</Checkbox>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const EditTaskButton = (props: IUpdateTaskProps) => {
  const [state, actions] = useTasks();

  const [visible, setVisible] = React.useState(false);

  const onCreate = (values: ITask) => {
    Api.Put(values)
      .then(() => actions.setRenew())
      .catch((ex) => {
        const error =
          ex.response.status === 404
            ? "Resource Not Found"
            : "An unexpected error has occured";
        actions.setError(error);
      });
    setVisible(false);
  };

  return (
    <div>
      <Button
        type="primary"
        onClick={() => {
          setVisible(true);
        }}
      >
        Edit
      </Button>
      <TaskEditForm
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          setVisible(false);
        }}
        task={props.task}
      />
    </div>
  );
};
