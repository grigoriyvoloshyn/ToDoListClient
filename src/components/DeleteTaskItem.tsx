import { Button } from "antd";
import { useTasks } from "../Store/Store";
import Api from "./Api";

interface IDeleteTaskItemProp {
  taskId: number;
}

export default function DeleteTaskItem(prop: IDeleteTaskItemProp) {
  const [state, actions] = useTasks();

  function onClickHandler() {
    Api.Delete(prop.taskId)
      .then(() => actions.setRenew())
      .catch((ex) => {
        const error =
          ex.response.status === 404
            ? "Resource Not Found"
            : "An unexpected error has occured";
        actions.setError(error);
      });
  }

  return (
    <Button onClick={() => onClickHandler()} type="primary" danger>
      Delete
    </Button>
  );
}
