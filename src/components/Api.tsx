import axios from "axios";
import { ITask, TaskList, ICreateTask } from "./Types";

function Get() {
  return axios.get<TaskList>("https://localhost:44331/api/Tasks", {
    headers: {
      "Content-Type": "application/json",
    },
  });
}

function GetById(id: number) {
  return axios.get<ITask>(`https://localhost:44331/api/Tasks/${id}`, {
    headers: {
      "Content-Type": "application/json",
    },
  });
}

function Post(values: ICreateTask) {
  return axios.post<ICreateTask>("https://localhost:44331/api/Tasks", values, {
    headers: {
      "Content-Type": "application/json",
    },
  });
}

function Put(values: ITask) {
  return axios.put<ITask>(
    `https://localhost:44331/api/Tasks/${values.id}`,
    {
      id: values.id,
      name: values.name,
      description: values.description,
      isCompleted: values.isCompleted,
    },
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
}

function Delete(taskId: number) {
  return axios.delete<TaskList>(`https://localhost:44331/api/Tasks/${taskId}`, {
    headers: {
      "Content-Type": "application/json",
    },
  });
}

export default { Get, Post, Put, Delete, GetById };
