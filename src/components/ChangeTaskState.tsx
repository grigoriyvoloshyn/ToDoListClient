import { Checkbox } from "antd";
import { IUpdateTaskProps } from "./Types";
import { useTasks } from "../Store/Store";
import Api from "./Api";

export default function ChangeTaskState(prop: IUpdateTaskProps) {
  const [state, actions] = useTasks();

  function onChangeHandler(e: any) {
    prop.task.isCompleted = e.target.checked;
    Api.Put(prop.task)
      .then(() => actions.setRenew())
      .catch((ex) => {
        const error =
          ex.response.status === 404
            ? "Resource Not Found"
            : "An unexpected error has occured";
        actions.setError(error);
      });
  }

  return (
    <Checkbox onChange={onChangeHandler} defaultChecked={prop.task.isCompleted}>
      Task completed
    </Checkbox>
  );
}
