import React from "react";
import { Card, Space, Row, Col } from "antd";
import TaskItem from "./TaskItem";
import { ITask } from "./Types";
import { useTasks } from "../Store/Store";
import Api from "./Api";

export default function ToDoList() {
  const [state, actions] = useTasks();

  React.useEffect(() => {
    Api.Get()
      .then((response) => {
        actions.setData(response.data);
      })
      .catch((ex) => {
        const error =
          ex.response.status === 404
            ? "Resource Not Found"
            : "An unexpected error has occured";
        actions.setError(error);
      });
  }, [state.Renew]);

  return (
    <Row>
      <Col className="gutter-row" span={8} offset={2}>
        <Card className="TaskList" title="Planned tasks">
          <Space
            className="scrollable-container"
            direction="vertical"
            size="middle"
            style={{ display: "flex" }}
          >
            {state.TaskList.map(
              (task: ITask) =>
                !task.isCompleted && (
                  <TaskItem key={task.id} task={task}></TaskItem>
                )
            )}
          </Space>
        </Card>
      </Col>
      <Col className="gutter-row" span={8} offset={2}>
        <Card className="TaskList" title="Finished tasks">
          <Space
            className="scrollable-container"
            direction="vertical"
            size="middle"
            style={{ display: "flex" }}
          >
            {state.TaskList.map(
              (task: ITask) =>
                task.isCompleted && (
                  <TaskItem key={task.id} task={task}></TaskItem>
                )
            )}
          </Space>
        </Card>
      </Col>
    </Row>
  );
}
