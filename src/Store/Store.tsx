import { createStore, createHook } from "react-sweet-state";
import { TaskList } from "../components/Types";

const defaultTasks: TaskList = [];

const Store = createStore({
  initialState: {
    TaskList: defaultTasks,
    Error: "",
    Renew: false,
  },

  actions: {
    setData:
      (data: TaskList) =>
      ({ setState }) => {
        setState({ TaskList: data });
      },
    setError:
      (error: any) =>
      ({ setState, getState }) => {
        const currentTasks = getState().TaskList;
        setState({ TaskList: currentTasks, Error: error });
      },
    setRenew:
      () =>
      ({ setState, getState }) => {
        let currentRenew = getState().Renew;
        setState({ Renew: currentRenew ? false : true });
      },
  },
});

export const useTasks = createHook(Store);

export default Store;
